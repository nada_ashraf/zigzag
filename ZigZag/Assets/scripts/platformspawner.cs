﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformspawner : MonoBehaviour
{
    float size;

    Vector3 lastPos;

    public GameObject Platform;

    public GameObject Diamond;

    public bool GameOver; 

    // Start is called before the first frame update
    void Start()
    {
        lastPos = Platform.transform.position;
        size = Platform.transform.localScale.x;

        for (int i = 0; i < 20; i++)
        {
            SpawnPlatforms(); 
        }

    }

    public void StartSpawnPlatform()
    {
        InvokeRepeating("SpawnPlatforms", 0.1f, 0.2f);

    }
    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.gameOver == true)
        {
            CancelInvoke("SpawnPlatforms"); 
        }

    }
    void SpawnPlatforms()
    {
        

        int rand = Random.Range(0,6);

        if (rand < 3)
        {
            spawnX(); 
        }
        else if (rand >= 3)
        {
            spawnZ(); 
        }

       

    }
    void spawnX()
    {
        GameObject newPlat; 
        Vector3 pos = lastPos;
        pos.x += size;
        lastPos = pos; 
        newPlat = Instantiate(Platform, pos, Quaternion.identity);

        int rand1 = Random.Range(0, 4);

        if (rand1 < 1)
        {
            Instantiate(Diamond, new Vector3(pos.x, pos.y + 1, pos.z), Diamond.transform.rotation);
        }
        
    }
    void spawnZ()
    {
        GameObject newPlat; 
        Vector3 pos = lastPos;
        pos.z += size;
        lastPos = pos;
        newPlat = Instantiate(Platform, pos, Quaternion.identity);

        int rand1 = Random.Range(0, 4);

        if (rand1 < 1)
        {
            Instantiate(Diamond, new Vector3 (pos.x , pos.y +1 , pos.z), Diamond.transform.rotation);
        }

    }
}
