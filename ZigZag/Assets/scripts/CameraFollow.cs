﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject ball;
    Vector3 offset;
    public bool Gameover; 
    public float lerpRate; 
    // Start is called before the first frame update
    void Start()
    {
        offset = ball.transform.position - transform.position;
        Gameover = false; 
    }

    // Update is called once per frame
    void Update()
    {
        if (!Gameover)
        {
            Follow(); 
        }
    }

    void Follow()
    {
        Vector3 pos = transform.position;
        Vector3 TargetPos = ball.transform.position - offset;
        pos = Vector3.Lerp(pos , TargetPos , lerpRate * Time.deltaTime);
        transform.position = pos; 
    }
}

