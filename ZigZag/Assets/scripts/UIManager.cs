﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager instance; 

    public GameObject ZigZagPanel;
    public GameObject GameOverPanel;
    public GameObject tabText; 
    public Text Score;
    public Text Score1;
    public Text Score2;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this; 
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Score1.text = " High Score: " + PlayerPrefs.GetInt("highScore").ToString();

    }



    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameStart()
    {

        

        tabText.SetActive(false); 

        ZigZagPanel.GetComponent<Animator>().Play("Panelup");
    }

    public void GameOver()
    {
        Score.text = PlayerPrefs.GetInt("Score").ToString();
        Score2.text = PlayerPrefs.GetInt("highScore").ToString();

        GameOverPanel.SetActive (true); 
    }

    public void Reset()
    {
        SceneManager.LoadScene(0); 
    }
}
