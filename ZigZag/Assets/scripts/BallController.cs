﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{


    [SerializeField]
    float Speed;

    Rigidbody rb;

    bool Started;
    bool GameOver;

    public GameObject Particle;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Started = false;
        GameOver = false; 
    }

    // Update is called once per frame
    void Update()
    {

        if (!Started)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rb.velocity = new Vector3(Speed, 0, 0);
                Started = true;


                GameManager.instance.StartGame(); 

            }
        }



        if (! Physics.Raycast(transform.position, Vector3.down, 1f))
        {
            GameOver = true;
            rb.velocity = new Vector3(0, -25f, 0);

            Camera.main.GetComponent<CameraFollow>().Gameover = true;

            GameManager.instance.GameOver(); 
        }


        if (Input.GetMouseButtonDown(0) && !GameOver )
        {
            SwichDirection(); 
           
        }
    }


    void SwichDirection()
    {
        if (rb.velocity.z > 0)
        {
            MoveX();
        }
        else if(rb.velocity.x > 0 )
        {
            MoveZ(); 
        }
    }
    void MoveX()
    {
        rb.velocity = new Vector3(Speed, 0, 0); 

    }

    void MoveZ()
    {
        rb.velocity = new Vector3(0, 0, Speed); 
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Diamond")
        {
           GameObject par =  Instantiate(Particle, col.gameObject.transform.position, Quaternion.identity) ;
            Destroy(col.gameObject);
            
            
        }
    }
}
